
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controlador1 implements ActionListener{
    
    private Vista1 ventana1;
    private String Nom;
    private String tel;

    public Controlador1(Vista1 ventana1) {
        
        this.ventana1 = ventana1;
       
        
    }
    
    public void iniciar(){
        
        ventana1.setTitle("Formulario");
        ventana1.setLocationRelativeTo(null);
        ventana1.setVisible(true);
        
        asignarControl();
    }
    
    private void asignarControl(){
        ventana1.getEnviar().addActionListener(this); 
    }
    
    private void Enviar(){
        Nom = ventana1.getNom().getText();
        tel = ventana1.getTel().getText();
        EventQueue.invokeLater(new Runnable(){
        
                public void run(){
                    new Controlador2(new vista2(),Nom, tel).iniciar();
                }
                      
        }
        );
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == ventana1.getEnviar()){
            Enviar();
        }
    }
    
    
    
    
    
}
