
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controlador2 implements ActionListener{
    
    private vista2 ventana2;
    private String nom;
    private String tel;

    public Controlador2(vista2 ventana2, String nom, String tel) {
        
        this.ventana2 = ventana2;
        this.nom = nom;
        this.tel = tel;
    }
    
    public void iniciar(){
        ventana2.setTitle("Datos de usuario");
        ventana2.setLocationRelativeTo(null);
        ventana2.setVisible(true);
        
        ventana2.getNom2().setText(nom);
        ventana2.getTel2().setText(tel);
        asignarControl();
    }
    
    public void asignarControl(){
        ventana2.getCerrar().addActionListener(this);
    }
    
    public void cerrar(){
        System.exit(0);
    }
    
    
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == ventana2.getCerrar()){
            cerrar();
        }
    }
    
    
}
